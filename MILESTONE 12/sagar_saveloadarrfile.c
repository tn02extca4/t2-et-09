#include <stdio.h>
#include <stdlib.h>
#include <sales.h>
#define SIZE 1000

int main()
{
    struct SalesPerson s[SIZE];

    //opening the file for reading
    FILE * openRead(char salesinfo[]);
    {
        FILE *fp;
        fp = fopen("salesinfo.txt", "r");

        int i;

        if(fp != NULL){

            //read from the file and save it to array s
            for (i=0;i<SIZE;i++){
                while(!feof(fp)){
                    fscanf(fp, "%d:%[^:]:%[^:]:%d:%lf\n" ,&s[i].salesNum,     s[i].lastName, s[i].firstName, &s[i].salesRate, &s[i].salesAmount);
                }
            }
            fclose(fp);
        }
        else
            printf("ERROR! Cannot open the file!\n");
    }
}
